###
The recorded messages by cues, and then save to a file.

[Redit]

[Author] Yuuki Zayasu<yukpiz@gmail.com>
###


# Send to roomid.
roomid = "145577_mine@conf.hipchat.com"

module.exports = (robot) ->

    robot.hear /COMMAND\?/i, (msg) ->
        # usage command.

    robot.hear /:MSGWATCH START (.*)/i, (msg) ->
        # starting message watch.
        status = robot.brain.data["#{roomid}_watchStatus"]
        status = true if !status?
        if status
            msg.send """
            既に*:msgwatch*が起動しています。
            *:msgwatch status*を実行して状態を確認してください。
            """


    robot.hear /:MSGWATCH SUSPEND/i, (msg) ->
        # suspending message watch.

    robot.hear /:MSGWATCH STOP/i, (msg) ->
        # stopping message watch.

    robot.hear /:MSGWATCH STATUS/i, (msg) ->
        # to display the status of message watch.

    robot.hear /(.*)/i, (msg) ->
        # recording message.



